# README #

Welcome to the Quick Start for JBehave project that will help you to get started using the JBehave tool with a
Java application that provides a RESTful web service to calculate the sum of two numbers. You can use this project as a
basis for specifying and verifying the behaviour of a Java application that needs to provide RESTful web services.

This project consists of two modules called *app* and *bdd*. The app module contains a Java application that provides a
RESTful web service to calculate the sum of two numbers. The bdd module contains a calculator story (calculator.story)
that is our single specification that describes the behaviour we want of the calculator application. We want the
calculator application to be able to calculate the sum of two numbers and we describe that behaviour through a single
scenario (example) where we ask the calculator what is the sum of 5 and 3 and we verify the calculator is able to give
us the correct answer 8.

### What is this repository for? ###

This repository is intended for developers to see how it is possible to turn a specification (calculator.story) into an
executable acceptance test that verifies a calculator has the behaviour to calculate the sum of two numbers.

### How do I get set up? ###

* Download and install Maven v3.2.1 or above
* Download and install JDK 7 or above

### How do I build and run the application? ###

* cd app
* mvn clean install
* mvn tomcat7:run

### How do I run the acceptance test using my favourite IDE? ###

In your IDE setup a run configuration to:

1. run the class com.stephenkurlow.calculator.runner.SelectiveJUnitStories
2. and give the configuration the VM option: -Dfilter="+SC_CALCULATOR_1"

The -Dfilter tells JBehave to only run the scenario (acceptance test) tagged with SC_CALCULATOR_1.

### Contribution guidelines ###

Contributions are gladly accepted to help the software development community to derive more value from this repository.
It is highly valued although not currently mandatory if in the application module you practise TDD (Test Driven Design).
Once you have a contribution ready to be pulled then create a pull request and it will be code reviewed. Once the code
review passes then your contribution will be merged into the master branch and a new release in the future will be
created.

### Who do I talk to? ###

The owner of this repository is Stephen Kurlow and he can be contacted at skurlow@gmail.com

### Professional Services ###

Stephen Kurlow is an experienced Accelerated Agile coach and is able to step a team up to become Behaviour Driven
Development (BDD) practitioners. He believes that building the right software the right way is what your business,
community and users expect from you and your team. You will learn to be able to specify the requirements and behaviour
of your software in the form of stories or features and have your favourite BDD tool be used to turn them into
executable acceptance tests that can repeatably verify the expected behaviour of your software in a Continuous
Integration (CI) environment using your favourite IDE, CI and programming language.

Contact Stephen Kurlow now on +61 433 560 440 or skurlow@gmail.com to get started on delivering better software today.
You and your team too can if the size of your software system is large be able to write 10000+ stories with 30000+
scenarios and have them execute in under 2 hours to prove your software is largely free of defects.