package com.stephenkurlow.calculator.runner;

import com.stephenkurlow.calculator.step.ExampleSteps;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;
import org.jbehave.core.Embeddable;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;
import static org.jbehave.core.reporters.Format.*;

/**
 * JUnit Runner that will run a single story that can be found on the classpath. The story that is chosen to be run is
 * named according to a convention and this class' name.
 */
@RunWith(JUnitReportingRunner.class)
public class ExampleScenarioJUnitStory extends JUnitStory {

	public ExampleScenarioJUnitStory() {
		JUnitReportingRunner.recommendedControls(configuredEmbedder());
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new ExampleSteps());
	}

	@Override
	public Configuration configuration() {
        Class<? extends Embeddable> embeddableClass = this.getClass();
		return new MostUsefulConfiguration().usePendingStepStrategy(new FailingUponPendingStep())
                .useStoryReporterBuilder(new StoryReporterBuilder()
                .withCodeLocation(codeLocationFromClass(embeddableClass))
                .withDefaultFormats()
                .withFormats(CONSOLE, TXT, HTML, XML));
	}
}
