package com.stephenkurlow.calculator.runner;

import com.stephenkurlow.calculator.step.CalculatorSteps;
import com.stephenkurlow.calculator.step.ExampleSteps;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;
import org.jbehave.core.Embeddable;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;
import static org.jbehave.core.reporters.Format.*;

/**
 * JUnit Runner that will run the stories or scenarios you wish to run according to their tag(s).
 */
@RunWith(JUnitReportingRunner.class)
public class SelectiveJUnitStories extends JUnitStories {

	public SelectiveJUnitStories() {
        final Embedder embedder = configuredEmbedder();
        final String filter = System.getProperty("filter");
        if (filter != null) {
            embedder.useMetaFilters(Arrays.asList(filter));
        }
        JUnitReportingRunner.recommendedControls(embedder);
	}

    @Override
    protected List<String> storyPaths() {
        return new StoryFinder().findPaths(codeLocationFromClass(this.getClass()), "**/*.story", "");
    }

    @Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new ExampleSteps(), new CalculatorSteps());
	}

	@Override
	public Configuration configuration() {
        Class<? extends Embeddable> embeddableClass = this.getClass();
		return new MostUsefulConfiguration().usePendingStepStrategy(new FailingUponPendingStep())
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withCodeLocation(codeLocationFromClass(embeddableClass))
                        .withDefaultFormats()
                        .withFormats(CONSOLE, TXT, HTML, XML));
	}
}
