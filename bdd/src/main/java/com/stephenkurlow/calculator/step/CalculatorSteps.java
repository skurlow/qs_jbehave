package com.stephenkurlow.calculator.step;

import com.stephenkurlow.calculator.model.CalculatorAddQuestion;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

/**
 * Class CalculatorSteps.
 */
public class CalculatorSteps {

    private ResponseEntity<String> stringResponseEntity;

    @Given("Calculator is ready")
    public void calculator_is_ready_to_start() {

    }

    @When("I ask the Calculator $number1 plus $number2")
    public void i_ask_the_calculator_number1_plus_number2(int number1, int number2) {

        final RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new MappingJackson2HttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
        final CalculatorAddQuestion calculatorAddQuestion = new CalculatorAddQuestion();
        calculatorAddQuestion.setNumber1(number1);
        calculatorAddQuestion.setNumber2(number2);
        stringResponseEntity = restTemplate.postForEntity("http://localhost:8080/app/calculator/addTwoNumbers", calculatorAddQuestion, String.class);
    }

    @Then("the Calculator tells me the answer is $answer")
    public void the_calculator_tells_me_the_answer_is(int answer) {
        assertThat(stringResponseEntity.getBody(), is(equalTo(String.valueOf(answer))));
    }
}
