Meta: @STORY_CALCULATOR

Narrative:
As a math student
I want to calculate the addition of two numbers
So that I can answer addition questions

Scenario: Add two whole numbers

Meta: @SC_CALCULATOR_1

Given Calculator is ready
When I ask the Calculator 5 plus 3
Then the Calculator tells me the answer is 8